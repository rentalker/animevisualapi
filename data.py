import pandas as pd
import ast
from pandas.core.frame import DataFrame
from datetime import datetime

date_format = '%Y-%m-%dT%H:%M:%S%z'

datasetUrl = 'http://sw.yazasnyal.ftp.sh:9004/share/Anime.csv'

anime_df = pd.read_csv(datasetUrl)

anime_df.dropna(inplace=True)
anime_df.drop(anime_df.index[anime_df[' rank']==0], inplace=True)

anime_new_df=anime_df.drop(columns=[' name', ' title_synonyms', ' status', ' airing',' synopsis', ' background', ' related', ' episodes', ' premiered', ' broadcast', ' duration'], axis=1)

def GetColumn(df: DataFrame, column_name: str):
    return df[column_name]

def FilterByTime(df: DataFrame, column_name: str, year_from: int, year_to: int):
    ans = []
    temp_df = df[' aired']
    i = -1
    for el in temp_df:
        i += 1
        el_dict = ast.literal_eval(el)
        # print('EL' + el, file=sys.stderr)
        temp_year_from = el_dict['from']
        temp_year_to = el_dict['to']
        if (temp_year_from != None) and (temp_year_to != None):
            temp_year_from = datetime.strptime(temp_year_from, date_format)
            temp_year_to =  datetime.strptime(temp_year_to, date_format)
            if not (((temp_year_from.year < year_from) and (temp_year_to.year < year_from)) or ((temp_year_from.year > year_to) and (temp_year_to.year > year_to))):
                ans.append(i)
        else:
            if (temp_year_from != None):
                temp_year_from =  datetime.strptime(temp_year_from, date_format)
                if (temp_year_from.year >= year_from) and (temp_year_from.year <= year_to):
                    ans.append(i)
            if (temp_year_to != None):
                temp_year_to =  datetime.strptime(temp_year_to, date_format)
                if (temp_year_to.year >= year_from) and (temp_year_to.year <= year_to):
                    ans.append(i)
    ans = df.iloc[ans]
    return ans[column_name]

def GetDataFull(df: DataFrame, year_from: int, year_to: int, head: int):
    ans = []
    temp_df = df[' aired']
    i = -1
    for el in temp_df:
        i += 1
        el_dict = ast.literal_eval(el)
        # print('EL' + el, file=sys.stderr)
        temp_year_from = el_dict['from']
        temp_year_to = el_dict['to']
        if (temp_year_from != None) and (temp_year_to != None):
            temp_year_from = datetime.strptime(temp_year_from, date_format)
            temp_year_to =  datetime.strptime(temp_year_to, date_format)
            if not (((temp_year_from.year < year_from) and (temp_year_to.year < year_from)) or ((temp_year_from.year > year_to) and (temp_year_to.year > year_to))):
                ans.append(i)
        else:
            if (temp_year_from != None):
                temp_year_from =  datetime.strptime(temp_year_from, date_format)
                if (temp_year_from.year >= year_from) and (temp_year_from.year <= year_to):
                    ans.append(i)
            if (temp_year_to != None):
                temp_year_to =  datetime.strptime(temp_year_to, date_format)
                if (temp_year_to.year >= year_from) and (temp_year_to.year <= year_to):
                    ans.append(i)
    ans = df.iloc[ans]
    if len(ans) <= head:
        return ans
    else: 
        ans = ans.sample(n=head)
        return ans

def PrepareColor(df, color: str):
    df_color = df[color]
    d = {}
    for el_color in df_color:
        arr = ast.literal_eval(el_color)
        for el in arr:
            if el in d:
                d[el] += 1
            else:
                d[el] = 1
    ans = list(dict(sorted(d.items(), key=lambda item: item[1])))[-5:]
    return ans

def CreateJsonFull(df, label: str, xaxis: str, yaxis: str, color: str):
    ans = {
        "label": df[label],
        "xaxis": df[xaxis],
        "yaxis": df[yaxis],
        "color": df[color],
        "topColors": PrepareColor(df, color),
        "size": df[" members"],
    }
    return ans
