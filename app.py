from fastapi.encoders import jsonable_encoder
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

import data

app = FastAPI()

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:3001",
    "https://localhost:3001",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)



@app.get("/anime-visual/column/{column_name}/from/{year_from}/to/{year_to}")
def GetDataYear(column_name: str, year_from: int, year_to: int):
    return jsonable_encoder(data.FilterByTime(data.anime_new_df, ' ' + column_name, year_from, year_to))

@app.get("/anime-visual/column/{column_name}")
def GetDataColumn(column_name: str):
    return jsonable_encoder(data.GetColumn(data.anime_new_df, ' ' + column_name))

@app.get("/anime-visual/label/{label}/xaxis/{xaxis}/yaxis/{yaxis}/color/{color}/from/{year_from}/to/{year_to}/head/{head}")
def GetDataFull(label: str, xaxis: str, yaxis: str, color: str, year_from: int, year_to: int, head: int):
    return data.CreateJsonFull(data.GetDataFull(data.anime_new_df, year_from, year_to, head), ' ' + label, ' ' + xaxis, ' ' + yaxis, ' ' + color)
